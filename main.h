#include <string_view>

#ifndef NDEBUG
#define ENABLE_LOGGING
#endif

#define POLICYTOOL_SIZEREQ(x,y) (offsetof(x,y) + sizeof(((x*) NULL)->y))
#define POLICYTOOL_ALIGN(x,y) ((((uintptr_t) (x)) + (((y)/CHAR_BIT)-1)) & ~(((y)/CHAR_BIT)-1))

extern char sDetourLibrary[512];

#ifdef ENABLE_LOGGING
void log_str(std::string_view str);
void log_str(std::wstring_view str);
void log_endl();
#else
#define log_str(...)
#define log_endl()
#endif
