#include <cstdio>
#include <windows.h>
#include <libloaderapi.h>



int main() {
    SetEnvironmentVariableW(L"__POLICYTOOL_ADMINIMPERSONATE", L"1");
    SetEnvironmentVariableW(L"__POLICYTOOL_REMOTELOCKBREAK", L"1");
    SetEnvironmentVariableW(L"__POLICYTOOL_POLICYDISABLE", L"1");

    auto dll = LoadLibraryW(L"libPolicyTool.dll");
    puts("Load success!");
}
