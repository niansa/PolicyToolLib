#include "../main.h"
#include "ModuleBase.hpp"

#include <windows.h>
#include <detours.h>



class LibInherit : public ModuleBase {
    inline static decltype(&CreateProcessA) TrueCreateProcessA;
    inline static decltype(&CreateProcessW) TrueCreateProcessW;

    static
    BOOL WINAPI DetourCreateProcessA(_In_opt_ LPCSTR lpApplicationName, _Inout_opt_ LPSTR lpCommandLine,
                                     _In_opt_ LPSECURITY_ATTRIBUTES lpProcessAttributes, _In_opt_ LPSECURITY_ATTRIBUTES lpThreadAttributes,
                                     _In_ BOOL bInheritHandles, _In_ DWORD dwCreationFlags, _In_opt_ LPVOID lpEnvironment,
                                     _In_opt_ LPCSTR lpCurrentDirectory, _In_ LPSTARTUPINFOA lpStartupInfo, _Out_ LPPROCESS_INFORMATION lpProcessInformation
    ) {
        char buffer[16];
        if (GetEnvironmentVariable("__POLICYTOOL_ADMINIMPERSONATE", buffer, sizeof(buffer)) && buffer[0] == '1') {
            SetEnvironmentVariableW(L"__COMPAT_LAYER", L"RunAsInvoker");
        }
        return DetourCreateProcessWithDllExA(lpApplicationName, lpCommandLine, lpProcessAttributes,
            lpThreadAttributes, bInheritHandles, dwCreationFlags, lpEnvironment, lpCurrentDirectory,
            lpStartupInfo, lpProcessInformation, sDetourLibrary, TrueCreateProcessA);
    }

    static
    BOOL WINAPI DetourCreateProcessW(_In_opt_ LPCWSTR lpApplicationName, _Inout_opt_ LPWSTR lpCommandLine,
                                     _In_opt_ LPSECURITY_ATTRIBUTES lpProcessAttributes, _In_opt_ LPSECURITY_ATTRIBUTES lpThreadAttributes,
                                     _In_ BOOL bInheritHandles, _In_ DWORD dwCreationFlags, _In_opt_ LPVOID lpEnvironment,
                                     _In_opt_ LPCWSTR lpCurrentDirectory, _In_ LPSTARTUPINFOW lpStartupInfo, _Out_ LPPROCESS_INFORMATION lpProcessInformation
    ) {
        char buffer[16];
        if (GetEnvironmentVariable("__POLICYTOOL_ADMINIMPERSONATE", buffer, sizeof(buffer)) && buffer[0] == '1') {
            SetEnvironmentVariableW(L"__COMPAT_LAYER", L"RunAsInvoker");
        }
        return DetourCreateProcessWithDllExW(lpApplicationName, lpCommandLine, lpProcessAttributes,
            lpThreadAttributes, bInheritHandles, dwCreationFlags, lpEnvironment, lpCurrentDirectory,
            lpStartupInfo, lpProcessInformation, sDetourLibrary, TrueCreateProcessW);
    }

public:
    LibInherit() {
        TrueCreateProcessA = CreateProcessA;
        TrueCreateProcessW = CreateProcessW;
        DetourAttach(&reinterpret_cast<PVOID&>(TrueCreateProcessA), reinterpret_cast<void*>(DetourCreateProcessA));
        DetourAttach(&reinterpret_cast<PVOID&>(TrueCreateProcessW), reinterpret_cast<void*>(DetourCreateProcessW));
    }
    ~LibInherit() {
        DetourDetach(&reinterpret_cast<PVOID&>(TrueCreateProcessA), reinterpret_cast<void*>(DetourCreateProcessA));
        DetourDetach(&reinterpret_cast<PVOID&>(TrueCreateProcessW), reinterpret_cast<void*>(DetourCreateProcessW));
    }
};
